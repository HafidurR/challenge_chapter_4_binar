const routes      = require('express').Router();
const userRoute   = require('../controller/userController');

routes.post('/register', userRoute.register);
routes.post('/login', userRoute.login);

module.exports = routes;