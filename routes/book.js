const router        = require('express').Router();
const bookRouter    = require('../controller/bookController');
const verifyToken   = require('../middlewares/verifyToken');

router.get('/', bookRouter.getAll);
router.get('/:id', verifyToken, bookRouter.getByID);
router.post('/', verifyToken, bookRouter.create);
router.put('/:id', verifyToken, bookRouter.update);
router.delete('/:id', verifyToken, bookRouter.delete);

module.exports = router;