const { User }            = require('../models');
const bcrypt              = require('bcrypt');
const jwt                 = require('jsonwebtoken');
const { JWT_SECRET_KEY }  = process.env;

class userController {
  static register = async (req, res) => {
    const { name, email, password, birthdate, no_telp, address, role } = req.body;

    const hash = await bcrypt.hash(password, 10);
    await User.create({
      name, email, password: hash, birthdate, no_telp, address, role
    })
      .then((result) => {
        return res.status(201).json({
          status: 'success',
          message: 'success register',
          data: result
        })
      })
      .catch((error) => {
        const err = error.errors
        // console.log(err);
        const errorList = err.map(d => {
          let obj = {}
          // console.log(obj[d.path]);
          obj[d.path] = d.message
          return obj;
        })
        return res.status(400).json({
          status: 'error',
          message: errorList
        })
      })
  }

  static login = async (req, res) => {
    const {
      email,
      password
    } = req.body;

    await User.findOne({
      where: {
        email
      }
    }).then(async result => {
      //return console.log(result);
      if (result === null) {
        return res.status(404).json({
          status: 'error',
          message: 'User not found'
        })
      } else {
        const data = {
          email: result.email,
          password: result.password,
          role: result.role
        }
        // return console.log(data);
        bcrypt.compare(password, result.password, async function (err, passed) {
          if (passed) {
            let token = jwt.sign(data, JWT_SECRET_KEY, {
              expiresIn: '1h'
            });
            return res.status(200).json({
              token
            })
          } else {
            return res.status(403).json({
              status: 'error',
              message: 'password is wrong'
            });
          }
        })
      }

    }).catch(error => {
      return res.status(400).json({
        status: 'error',
        message: error.message
      })
    })
  }
}

module.exports = userController;