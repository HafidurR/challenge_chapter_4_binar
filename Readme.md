### My Repository to clone

https://gitlab.com/HafidurR/challenge_chapter_4_binar
### Installation

1. Clone the repo
2. Install NPM packages
   ```sh
   npm install
   ```

## Create Database and Table

- run node `sequelize db:migrate`

## Seeding Data

- run node `sequelize db:seed:all`

##  Run the Program

- node `app.js` 

##  Test the Program

- Import to postman `Challenge - Binar.postman_collection.json` 