module.exports = (sequelize, Datatypes) => {
  const Author = sequelize.define('Author', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Datatypes.INTEGER
    },
    name: {
      type: Datatypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Name is required'
        },
        notEmpty: {
          args: true,
          msg: "Name cannot be empty",
        }
      }
    },
    birthdate: {
      type: Datatypes.DATE,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Birthdate is required'
        },
        notEmpty: {
          args: true,
          msg: "Birthdate cannot be empty",
        }
      }
    },
    gender: {
      type: Datatypes.ENUM,
      values: ['pria', 'wanita'],
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Gender is required'
        },
        notEmpty: {
          args: true,
          msg: "Gender cannot be empty",
        }
      }
    },
    address: {
      type: Datatypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Address is required'
        },
        notEmpty: {
          args: true,
          msg: "Address cannot be empty",
        }
      }
    },
    createdAt: {
      allowNull: false,
      type: Datatypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: Datatypes.DATE
    }
  }, {
    tableName: 'authors'
  })

  Author.associate = models => {
    Author.hasMany(models.Book, {
      foreignKey: 'author_id'
    })
  }
  return Author;
}
