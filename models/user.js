module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        notNull: {
          msg: 'Name is required'
        },
        notEmpty: {
          args: true,
          msg: "Name cannot be empty",
        }
      }
    },
    birthdate: {
      allowNull: false,
      type: DataTypes.DATE,
      validate: {
        notNull: {
          msg: 'Birthdate is required'
        },
        notEmpty: {
          args: true,
          msg: "Birthdate cannot be empty",
        }
      }
    },
    no_telp: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        notNull: {
          msg: 'No_telp is required'
        },
        notEmpty: {
          args: true,
          msg: "No_telp cannot be empty",
        }
      }
    },
    address: {
      allowNull: false,
      type: DataTypes.TEXT,
      validate: {
        notNull: {
          msg: 'Address is required'
        },
        notEmpty: {
          args: true,
          msg: "Address cannot be empty",
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: {
          args: true,
          msg: "Invalid email format",
        },
        notNull: {
          msg: "Email required",
        },
        notEmpty: {
          args: true,
          msg: "Email cannot be empty",
        },
      },
      unique: {
        args: true,
        msg: "Email already registered, use another email",
      },
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        notNull: {
          msg: 'Password is required'
        },
        notEmpty: {
          args: true,
          msg: "Password cannot be empty",
        }
      }
    },
    role: {
      type: DataTypes.ENUM,
      values: ['admin', 'user'],
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Role is required'
        },
        notEmpty: {
          args: true,
          msg: "Role cannot be empty",
        }
      }
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    tableName: 'users'
  })

  return User;
}