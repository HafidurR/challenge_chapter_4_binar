module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    category_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Category_name is required'
        },
        notEmpty: {
          args: true,
          msg: "Category_name cannot be empty",
        }
      }
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    tableName: 'categories'
  })
  Category.associate = models => {
    Category.hasMany(models.Book, {
      foreignKey: 'category_id'
    });
  }
  return Category;
}