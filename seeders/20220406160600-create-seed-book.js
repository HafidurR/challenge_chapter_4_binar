'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('books', [{
      title: 'Aku dimana ini ?',
      description: 'Nothing to describe',
      publisher: 'Gramedia',
      publish_at: '2020-10-10',
      author_id: 1,
      category_id: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      title: 'Harry Potter',
      description: 'Nothing to describe',
      publisher: 'Gramedia',
      publish_at: '2010-12-12',
      author_id: 2,
      category_id: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      title: 'One Piece Stampade',
      description: 'Nothing to describe',
      publisher: 'Gramedia',
      publish_at: '2016-09-30',
      author_id: 3,
      category_id: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('authors', null, {});
     */
    await queryInterface.bulkDelete('books', null, {});
  }
};
