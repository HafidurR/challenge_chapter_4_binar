'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('users', [{
      name: 'M. Hafidurrohman',
      email: 'hafid@mail.com',
      password: bcrypt.hashSync('hafid123', 10),
      birthdate: '2001-04-04',
      no_telp: '089241887541',
      address: 'Probolinggo Jawa Timur',
      role: 'admin',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Avin Abdurrohim',
      email: 'avin@mail.com',
      password: bcrypt.hashSync('avin123', 10),
      birthdate: '2000-10-04',
      no_telp: '082241887713',
      address: 'Situbondo Jawa Timur',
      role: 'user',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('authors', null, {});
     */
    await queryInterface.bulkDelete('users', null, {});
  }
};
